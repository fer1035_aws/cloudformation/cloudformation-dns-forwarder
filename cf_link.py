#!/usr/bin/env python3
# -*- coding: latin-1 -*-
"""
Create a quick link for a CloudFormation template.

Link template:
    https://console.aws.amazon.com/cloudformation/home
    ?region=<region>#/stacks/quickcreate?templateURL=<s3-url>
    &stackName=<stack-name>
    &param_<parameter-name>=<parameter-value>
    &param_<parameter-name>=<parameter-value>

Prerequisites:
    - The S3 object must be accessible to the deploying AWS account.
"""

import argparse
import re
from typing import Optional, Any


__version__ = '1.0.0'
__author__ = 'Ahmad Ferdaus Abd Razak'
__status__ = 'Production'


def validate_input(
    input: Any,
    pattern: str,
    error: str
) -> None:
    """Validate the input based on the input pattern."""
    if (not re.match(pattern, input)):
        raise ValueError(error)


def create_cf_link(
    s3_url: str,
    stack_name: Optional[str],
    parameters: Optional[list]
) -> str:
    """Create a quick link for a CloudFormation template."""
    # Format the link.
    link = (
        'https://console.aws.amazon.com/cloudformation/home?#/'
        'stacks/create/review'
        f'?templateURL={s3_url}'
    )

    # If stack name is provided, add it to the link.
    if stack_name:
        link = f'{link}&stackName={stack_name}'

    # If parameters are provided, add them to the link.
    if parameters:
        # Process parameters.
        parameter_string = '&'.join(
            f'param_{parameter.split("=")[0]}={parameter.split("=")[1]}'
            for parameter in parameters
        )
        link = f'{link}&{parameter_string}'

    return link


def main():
    """Execute the main function."""
    # Get command-line arguments.
    parser = argparse.ArgumentParser(
        add_help=True,
        allow_abbrev=False,
        description='Create a quick link for a CloudFormation template.',
        usage='%(prog)s [options]'
    )
    parser.add_argument(
        '-V',
        '--version',
        action='version',
        version=f'%(prog)s {__version__}',
        help='Show the version number and exit.'
    )
    parser.add_argument(
        '-s',
        '--s3_url',
        action='store',
        help='S3 URL of the CloudFormation template.',
        required=True,
        type=str
    )
    parser.add_argument(
        '-n',
        '--stack_name',
        action='store',
        help='[OPTIONAL] Name of the CloudFormation stack.',
        required=False,
        default=None,
        type=str
    )
    parser.add_argument(
        '-p',
        '--parameters',
        action='append',
        help=(
            '[OPTIONAL] Parameters for the CloudFormation template.'
            ' This option can be used multiple times in the following'
            ' format: -p key=value'
        ),
        required=False,
        default=None,
        type=str
    )
    args = parser.parse_args()

    # Validate inputs.
    validate_input(
        args.s3_url,
        r'^https:\/\/.*s3\..*[a-z]+\-[a-z]+\-[0-9]+\.amazonaws\.com\/.+$',
        'Invalid S3 URL format.'
    )
    if args.stack_name:
        validate_input(
            args.stack_name,
            r'^[a-zA-Z0-9\-]+$',
            'Invalid stack name format.'
        )
    if args.parameters:
        for parameter in args.parameters:
            validate_input(
                parameter,
                r'^[\w\-]+\=[\w\-]+$',
                f'Invalid parameter: {parameter}.'
            )

    # Create the quick link.
    link = create_cf_link(
        args.s3_url,
        args.stack_name,
        args.parameters
    )

    print(link)


if __name__ == '__main__':
    main()
