# cloudformation-aws-dns-forwarder

An AWS CloudFormation template to deploy private DNS forwarders for on-prem or inter-VPC resolution.

![DNS query between private networks](images/diagram.png)

## Resources

- Route 53
    - Outbound endpoint
    - Resolver rule
- VPC
    - Security group

## Prerequisites

- An existing VPC with 2 private subnets in which to create the forwarder.
- At least 1 DNS server / domain controller to use as the target.
- Network connection from within the private subnets to the DNS server.

## Installation Options

- [Login](https://console.aws.amazon.com) to your AWS account and clink the [quick link](https://console.aws.amazon.com/cloudformation/home?#/stacks/create/review?templateURL=https://warpedlenses-public.s3.ap-southeast-1.amazonaws.com/cloudformation/resolver.yaml) to deploy in CloudFormation (recommended), or

- Download the [template](cloudformation/resolver.yaml) and deploy manually in the CloudFormation [console](https://console.aws.amazon.com/cloudformation), especially if you'd like to customize it.

## Testing

- Launch an EC2 instance inside one of the private subnets.
- Try to resolve a orivate hostname in the target network.
    ```bash
    nslookup <hostname.private.network>
    ```
