#!/usr/bin/env bash

# Get file name.
FILE_NAME="$(ls cloudformation/)"

# Upload to S3.
AWS_ACCESS_KEY_ID="$AWS_ACCESS_KEY_ID" AWS_SECRET_ACCESS_KEY="$AWS_SECRET_ACCESS_KEY" aws s3 cp "cloudformation/$FILE_NAME" "s3://warpedlenses-public/cloudformation/$FILE_NAME"

# Get the link.
python3 cf_link.py -s "https://warpedlenses-public.s3.ap-southeast-1.amazonaws.com/cloudformation/$FILE_NAME"
